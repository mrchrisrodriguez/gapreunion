# Gap Reunion Tests

There are a couple of steps required to run the different specs for Gap Reunion as described below:


## Acceptance Tests

In order to run acceptance tests (those that test the web perspective) also known as feature tests, you should install **PhantomJS**. On the poltergeist gem README there are some basic instructions on how to do so ( https://github.com/jonleighton/poltergeist ). If you have HomeBrew installed on your mac then it is as easy as running `brew install phantomjs`

You will also have to have the Stripe API key set for these tests to work. At some point we may mock out the calls so that the tests won't hit the Stripe servers.

## Running Tests

You can run the tests by executing either `rake spec` or `guard start`. Rake spec is the traditional way of executing the test suite, and guard start is a autotest runner that runs tests as you modify your code (useful for active development). Either way works.