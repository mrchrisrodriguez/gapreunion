FactoryGirl.define do
  factory :admin_user do
    email { Faker::Internet.email }
    password "passw0rd"
  end
end