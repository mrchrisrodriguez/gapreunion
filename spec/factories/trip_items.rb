# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :trip_item do
    trip
    title { Faker::Lorem.sentence(3) }
    body { Faker::Lorem.paragraph }
  end

  factory :key_point_item, parent: :trip_item, class: KeyPointItem do
  end
  factory :other_point_item, parent: :trip_item, class: OtherPointItem do
  end
  factory :faq_item, parent: :trip_item, class: FaqItem do
  end
end
