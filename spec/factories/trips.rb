# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :trip do
    name { city + " University Young Alumni Reunion"}
    code { name.scan(/[A-Z]/).join.downcase }
    price { rand(25..200) }
    start_date { rand((Date.today + 1.week)..(Date.today + 1.year)) }
    end_date { start_date + 2.days }
    city { Faker::Address.city }
    state { Faker::AddressUS.state }
    goal { rand(50..200) }
  end
end
