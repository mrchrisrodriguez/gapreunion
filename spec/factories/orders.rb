FactoryGirl.define do
  factory :order do
    name { Faker::Address.city + " Alumni Reunion" }
    price { rand(25.0..200.0) }
    user
    trip
  end
end