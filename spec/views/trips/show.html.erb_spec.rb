require 'spec_helper'

describe "trips/show" do
  it "displays the trip info" do
    assign(:trip, trip = FactoryGirl.create(:trip))
    10.times do
      FactoryGirl.create(:trip_item, :type => TripItem.item_types.sample, :trip => trip)
    end
    render
    expect(rendered).to have_content ('in ' + trip.city)
    expect(rendered).to have_content(trip.human_price)
    expect(rendered).to have_content trip.start_date.strftime('%B %d') + '-' + trip.end_date.strftime('%d, %Y')
    expect(rendered).to_not have_content('MIT') # make sure mit does not come up (not using settings)
    trip.trip_items.each do |item|
      expect(rendered).to have_content item.title
      expect(rendered).to have_content item.body
    end
  end
end