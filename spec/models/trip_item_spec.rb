describe TripItem do
  it{should belong_to :trip}
  it{should validate_presence_of :trip_id}
  it{should validate_presence_of :type}
  it{should validate_presence_of :title}
  it{should validate_presence_of :body}

  it "sets position after saved" do
    trip = FactoryGirl.build(:faq_item)
    expect(trip.position).to be_nil
    trip.save
    expect(trip.position).to_not be_nil
  end
  it "can be scoped by type" do
    2.times do
      FactoryGirl.create(:faq_item)
      FactoryGirl.create(:other_point_item)
    end
    trip_items = TripItem.with_type(:faq).all
    expect(trip_items.size).to equal(2)
    trip_items.each{|t| expect(t).to be_a(FaqItem)}
  end
end
describe FaqItem do
  it_behaves_like "a model"
end
describe KeyPointItem do
  it_behaves_like "a model"
end
describe OtherPointItem do
  it_behaves_like "a model"
end