require 'spec_helper'

describe Order do
  it_behaves_like "a model"

  it {should belong_to :trip}
  it {should validate_presence_of :user_id}
  it {should validate_presence_of :trip_id}
  it {should validate_presence_of :name}
  it {should validate_presence_of :price}

end