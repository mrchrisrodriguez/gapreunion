require 'spec_helper'

describe Trip do
  it_behaves_like "a model"

  it { should have_many(:trip_items) }
  it { should have_many(:orders) }
  it { should validate_presence_of(:name) }
  it { should validate_numericality_of(:price) }
  it { should validate_numericality_of(:goal) }
  it { should validate_uniqueness_of(:code) }

  it "validates the start and end trip dates" do
    t = FactoryGirl.create(:trip)
    expect(t).to allow_value(Date.today).for(:start_date).for(:end_date)
    expect(t).to_not allow_value("Sometime in February").for(:start_date).for(:end_date)
  end

  it "validates expiration date" do
    t = FactoryGirl.build(:trip)
    expect(t).to allow_value(t.start_date).for(:expiration_date)
    expect(t).to allow_value(t.end_date - 1.year).for(:expiration_date)
    expect(t).to_not allow_value(t.end_date + 1.day).for(:expiration_date)
  end

  it "has trip items" do
    trip = FactoryGirl.create(:trip)
    8.times do
      FactoryGirl.create([:faq_item, :key_point_item, :other_point_item].sample, :trip => trip)
    end
    expect(trip).to have(8).trip_items
    expect(trip).to have(FaqItem.count).faq_items
    expect(trip).to have(KeyPointItem.count).key_point_items
    expect(trip).to have(OtherPointItem.count).other_point_items
  end

  it "has a goal and completion percent" do
    t = FactoryGirl.create(:trip)
    expect(t).to_not allow_value(-1).for(:goal)
    t.goal = 100
    mock(t).attending.times(2){50}
    expect(t.goal_percent).to eq(50.0)
    expect(t.remaining).to eq(50)
  end
  it "cannot have negative remaining" do
    t = FactoryGirl.create(:trip)
    mock(t).attending{t.goal + 20}
    expect(t.remaining).to eq(0)
  end
  it "can calculate price in cents" do
    t = FactoryGirl.create(:trip)
    expect(t.price_in_cents).to eq(t.price * 100)
  end
end