require 'spec_helper'

describe User do
  it_behaves_like "a model"

  it{ should have_many :orders }
  it {should ensure_inclusion_of(:gender).in_array(['male', 'female'])}
end