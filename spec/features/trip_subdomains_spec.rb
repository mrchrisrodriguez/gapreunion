require 'features/features_helper'

feature 'Trip Sub Domains', %q{
  In order to join certain trips
  As an ex-alumni grownup
  I want to be able to find my alumni trip by its subdomain
} do

  let(:trip) do
    FactoryGirl.create(:trip)
  end

  scenario 'Go to my alumni trip page as a non-registered user and checkout', :js => true do
    trip # make sure trip gets created
    visit url_for_subdomain trip.code, trip_path
    expect(page).to have_following_content ("in " + trip.city), 
      trip.name, trip.human_price
    click_link 'Book Now'
    expect(page).to have_following_content trip.name, "Check out"
    within('#checkout') do
      fill_in 'Name', with: Faker::Name.name
      fill_in 'Email', with: Faker::Internet.email
      page.execute_script "$('#amazon_button').removeClass('disabled')"
      select ['Male', 'Female'].sample, from: 'gender'
      fill_in 'Address line 1', with: Faker::Address.street_address
      fill_in 'Zipcode', with: Faker::AddressUS.zip_code
      fill_in 'Credit Card number', with: '4242424242424242'
      fill_in 'Expiry month (e.g. 3)', with: '3'
      fill_in 'Expiry year (e.g. 2015)', with: (Date.today + 2.years).year.to_s
      fill_in 'CVC', with: '123'
    end
    click_button "Checkout"
    wait_up_to(4) do # wait until stripe callback finishes and following post to server
      expect(page).to have_content "Hooray! You've just reserved a #{trip.name}!"
    end
    expect(page).to have_following_content "Congratulations, you're attendee number " + Order.last.number,
        "Reservation ID: " + Order.last.uuid
  end
end