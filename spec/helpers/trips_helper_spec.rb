require 'spec_helper'
describe TripsHelper do
  describe '.trip_dates' do
    it 'displays day range for dates in same month' do
      assign(:trip, trip = FactoryGirl.build(:trip, :start_date => Date.new(2013, 3, 1), :end_date => Date.new(2013, 3, 3)))
      expect(helper.trip_dates).to eq("March 1-3, 2013")
    end
    it 'displays month range for dates in same year' do
      assign(:trip, trip = FactoryGirl.build(:trip, :start_date => Date.new(2013, 3, 30), :end_date => Date.new(2013, 4, 2)))
      expect(helper.trip_dates).to eq("March 30 - April 2, 2013")
    end
    it 'displays full range for dates in different years' do
      assign(:trip, trip = FactoryGirl.build(:trip, :start_date => Date.new(2012, 12, 30), :end_date => Date.new(2013, 1, 4)))
      expect(helper.trip_dates).to eq("December 30, 2012 - January 4, 2013")
    end
  end
end