require 'spec_helper'

describe "routing to trips" do
  it "routes /go to trip#show for current trip based on subdomain" do
    expect(:get => url_for_subdomain("mit", "/go")).to route_to(
      :controller => "trips",
      :action => "show"
    )
  end
  it "routes /checkout to orders controller for current trip" do
    expect(:get => url_for_subdomain("mit", "/checkout")).to route_to(
      :controller => "orders",
      :action => "new"
      )
  end
  it "routes /go to home on default domain" do
    expect(:get => "/go").not_to be_routable
  end
end