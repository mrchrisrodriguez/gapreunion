Selfstarter::Application.routes.draw do
  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  # This must come before general routes to ensure proper redirects
  constraints(TripSubdomain) do
    match "/" => redirect("/go")
    match "go" => "trips#show", :as => :trip
    get "checkout" => "orders#new"
    match 'checkout' => "orders#create", :via => :post
    match "orders/:id" => "orders#show", :as => :order
  end

  root :to => "home#index"
  match "/go" => redirect("/"), :constraints => {:subdomain => 'www'}
  match "/checkout" => redirect("/"), :constraints => {:subdomain => 'www'}
end
