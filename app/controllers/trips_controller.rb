class TripsController < ApplicationController
  # skip_before_filter :verify_authenticity_token, :only => :ipn

  before_filter :set_page_title
  before_filter :set_trip

  def show
  end

  def checkout!
    @user = User.find_or_create_by_email!(params[:email])
    redirect_to root_url unless params[:stripe_token]

    # Create a Stripe customer that we can charge later, and
    # attach the customer ID to the User object.
    customer = Stripe::Customer.create(
      :description => "Customer for #{params[:email]}",
      :email => params[:email],
      :card => params[:stripe_token]
    )

    # Create an order for this user.
    @order = Order.generate
    @order.stripe_customer_id = customer.id
    @order.name = Settings.product_name
    @order.price = Settings.trip.price
    @order.user_id = @user.id
    @order.address_line1 = params[:address_line1]
    @order.address_line2 = params[:address_line2]
    @order.city = params[:city]
    @order.state = params[:state]
    @order.phone = params[:phone]
    @order.zip = params[:address_zip]
    @order.country = params[:country]
    @order.save!

    redirect_to root_url
  end

  def share
    @order = Order.find_by_uuid(params[:uuid])
  end

  private
  # TODO: Refactor to use i18n
  def set_page_title
    @page_title = current_trip.name
  end
  def set_trip
    @trip = current_trip
  end
end