class OrdersController < ApplicationController

  def new
  end
  # TODO: Get rid of unnecessary code
  def create
    @user = User.find_or_create_by_email!(params[:email])
    redirect_to '/' unless params[:stripe_token]

    # Create a Stripe customer that we can charge later, and
    # attach the customer ID to the User object.
    # 
    if @user.stripe_customer_id.blank?
      customer = Stripe::Customer.create(
        :description => "Customer for #{params[:email]}",
        :email => params[:email],
        :card => params[:stripe_token]
      )
      @user.stripe_customer_id = customer.id
      @user.save
    end

    @user.update_attributes(gender: params[:gender], name: params[:name])

    charge = Stripe::Charge.create(
      :description => "Charge for #{params[:email]}",
      :currency => "usd",
      :amount => current_trip.price_in_cents,
      :customer => @user.stripe_customer_id,
      :capture => Settings.stripe_capture_payment
    )

    # Create an order for this user.
    # TODO: Use mass-assign params instead
    @order = Order.generate
    @order.stripe_charge_id = charge.id
    @order.name = current_trip.name
    @order.price = current_trip.price
    @order.user = @user
    @order.trip = current_trip
    @order.address_one = params[:address_line1]
    @order.address_two = params[:address_line2]
    @order.city = params[:city]
    @order.state = params[:state]
    @order.phone = params[:phone]
    @order.zip = params[:address_zip]
    @order.country = params[:country]
    @order.save!

    redirect_to order_url(@order)
  end
  def show
    @order = Order.find(params[:id])
  end
end