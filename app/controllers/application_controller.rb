class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :valid_trip

  private

  def valid_trip
    if TripSubdomain.matches?(request)
      redirect_to request.url.sub(request.subdomain, 'www') if current_trip.nil?
    end
  end
  def current_trip
    @current_trip ||= TripSubdomain.matches?(request) ? Trip.find_by_code(request.subdomain.downcase) : nil
  end

end
