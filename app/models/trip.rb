class Trip < ActiveRecord::Base
  attr_accessible :city, :end_date, :name, :price, :start_date, :expiration_date, :state, :code, :goal

  has_many :trip_items
  has_many :orders
  has_many :attendees, :through => :orders, :source => :user

  validates :name, :presence => true
  validates :price, :numericality => {:precision => 8, :scale => 2, :allow_nil => true}
  validates :code, :uniqueness => true
  validates :goal, :numericality => {:greater_than_or_equal_to => 0}
  validates :start_date, :date => {:before_or_equal_to => :end_date}, :allow_nil => true
  validates :end_date, :date => {}, :allow_nil => true
  validates :expiration_date, :date => {:before_or_equal_to => :end_date}, :allow_nil => true

  def human_price
    "Only $" + price.to_s
  end

  def price_in_cents
    (price * 100).to_i
  end

  def faq_items
    trip_items.with_type(:faq)
  end
  def key_point_items
    trip_items.with_type(:key_point)
  end
  def other_point_items
    trip_items.with_type(:other_point)
  end
  def attending
    attendees.count
  end
  def remaining
    remain = goal - attending
    remain < 0 ? 0 : remain
  end
  def goal_percent
    (attending.to_f / goal.to_f) * 100.to_f
  end
  def revenue
    attending.to_f * price
  end
end
