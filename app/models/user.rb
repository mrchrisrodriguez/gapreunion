class User < ActiveRecord::Base
  extend Enumerize
  attr_accessible :name, :email, :gender
  has_many :orders

  enumerize :gender, in: [:female, :male]
  validates :email, :presence => true
end
