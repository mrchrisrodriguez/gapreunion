class TripItem < ActiveRecord::Base
  extend Enumerize
  attr_accessible :body, :title, :trip_id, :type

  belongs_to :trip
  acts_as_list scope: [:trip_id, :type]

  validates :title, :presence => true
  validates :body, :presence => true
  validates :type, :presence => true, :inclusion => ['FaqItem', 'KeyPointItem', 'OtherPointItem']
  validates :trip_id, :presence => true

  scope :with_type, lambda{|t|
    type_class = t.to_s.classify + 'Item'
    where(:type => type_class)
  }

  default_scope order: :position

  def self.item_types
    ['KeyPointItem', 'OtherPointItem', 'FaqItem']
  end
end
