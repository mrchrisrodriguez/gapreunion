class Order < ActiveRecord::Base
  # Address details.
  attr_accessible :address_one, :address_two, :city, :state, :country, :zip, :phone
  # Shipping/billing details.
  attr_accessible :number, :shipping, :tracking_number, :stripe_customer_id
  # Product details.
  attr_accessible :name, :price
  attr_readonly :uuid

  before_validation :generate_uuid!, :on => :create
  belongs_to :user
  belongs_to :trip
  self.primary_key = 'uuid'

  validates_presence_of :name, :price, :user_id, :trip_id

  scope :recent, lambda{|n=5| order(:created_at).limit(n) }

  def self.generate
  	o = self.new
  	o.number = Order.next_number || 1
    o
  end

  def self.next_number
  	Order.order("number DESC").limit(1).first.number.to_i + 1 if Order.count > 0
  end

  def generate_uuid!
  	self.uuid = SecureRandom.hex(16)
  end

end
