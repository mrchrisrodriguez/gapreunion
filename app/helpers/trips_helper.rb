module TripsHelper
  def trip_dates
    start_date = @trip.start_date
    end_date = @trip.end_date
    if start_date.year != end_date.year
      start_date.strftime('%B %-d, %Y - ') + end_date.strftime('%B %-d, %Y')
    elsif start_date.month != end_date.month
      start_date.strftime('%B %-d - ') + end_date.strftime('%B %-d, %Y')
    else
      start_date.strftime('%B %-d-') + end_date.strftime('%-d, %Y')
    end
  end
end