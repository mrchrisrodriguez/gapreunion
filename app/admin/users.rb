ActiveAdmin.register User do
  index do
    selectable_column
    column :id
    column :name
    column :email
    column :gender
    column 'Stripe Customer' do |user|
      link_to('Customer', 'http://manage.stripe.com/customers/' + user.stripe_customer_id, :class => 'member_link')
    end
    column :created_at
    default_actions
  end
end
