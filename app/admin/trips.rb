ActiveAdmin.register Trip do
  index do
    selectable_column
    column :id
    column :name
    column :code
    column :price
    column :start_date
    column :end_date
    column :goal
    column :current do |trip|
      trip.attendees.count
    end
    column 'Trip Items' do |trip|
      link_to "View Trip Items", [:admin, trip, :trip_items]
    end
    default_actions
  end
end
