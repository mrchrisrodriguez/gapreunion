ActiveAdmin.register TripItem do
  belongs_to :trip

  config.sort_order = 'position_asc'
  config.paginate = false

  sortable

  index do      
    sortable_handle_column
    selectable_column
    column :id  
    column :type                    
    column :title                    
    column :body, :sortable => false do |trip_item|
      truncate(trip_item.body, :length => 200)
    end
    column :position             
    default_actions                   
  end   

  filter :type, :as => :select, :collection => TripItem.item_types
  filter :title
  filter :body

  form do |f|                         
    f.inputs "Trip Item Details" do       
      f.input :type, :as => :select, :collection => TripItem.item_types, :include_blank => false
      f.input :title               
      f.input :body
    end                               
    f.actions                         
  end             
end
