ActiveAdmin.register Order do

  config.sort_order = 'created_at_desc'

  index do 
    selectable_column
    column :uuid
    column :trip
    column :number
    column :user
    column :price
    column "Date", :created_at
    column 'Stripe' do |order|
      raw(link_to('Customer', 'http://manage.stripe.com/customers/' + order.user.stripe_customer_id, :class => 'member_link') + 
        link_to('Payment', 'http://manage.stripe.com/payments/' + order.stripe_charge_id,  :class => 'member_link'))
    end
    default_actions
  end
  controller do
    def scoped_collection
      Order.includes(:user)
    end
  end
  form do |f|                         
    f.inputs "Order Details" do       
      f.input :trip, :required => true
      f.input :user, :required => true
      f.input :name
      f.input :price
      f.input :address_one
      f.input :address_two
      f.input :city
      f.input :state
      f.input :zip
      f.input :number
      f.input :phone
      f.input :expiration
    end                               
    f.actions                         
  end
end
