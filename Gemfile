source 'https://rubygems.org'

gem 'rails', '3.2.13'

group :production do
	gem 'thin'
	gem 'pg'
end

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.6'
  gem 'coffee-rails', '~> 3.2.2'
  gem 'therubyracer', :platforms => :ruby
  gem 'uglifier', '>= 1.3.0'
end

# jQuery
gem 'jquery-rails'

# Kickstarter's awesome Amazon Flexible Payments gem
gem 'amazon_flex_pay'

# Stripe gem
gem 'stripe'

# Configuration File
gem 'rails_config'

# Authentication
gem 'devise', '~> 2.2.3'

# Validations and Model Enhancements
gem 'date_validator', "~> 0.6.4"
gem 'enumerize', "~> 0.5.1"
gem 'acts_as_list', "~> 0.2.0"

# Admin gems
gem 'activeadmin'
gem 'activeadmin-sortable'

group :development do
  gem 'sqlite3'
  gem 'letter_opener'

  # Speed and testing Tools (not needed on testing/integration server)
  gem 'guard'
  gem 'rb-fsevent'
  gem 'zeus'
  gem 'guard-rspec'

  # Debugger and REPL
  gem 'pry'
  gem 'pry-stack_explorer'
  gem 'pry-debugger'
end

group :test, :development do
  gem 'rspec'
  gem 'rspec-rails', ">= 2.0"
  gem 'shoulda-matchers'
  gem 'factory_girl', ">= 4.2.0"
  gem 'factory_girl_rails'
  gem 'capybara'
  gem 'poltergeist'
  gem 'rr'
  gem 'ffaker'
  gem 'database_cleaner'

  gem 'ruby_gntp'
  gem 'fuubar'
end