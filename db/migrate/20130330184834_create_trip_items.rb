class CreateTripItems < ActiveRecord::Migration
  def change
    create_table :trip_items do |t|
      t.string :title
      t.text :body
      t.string :type
      t.integer :position
      t.references :trip

      t.timestamps
    end
  end
end
