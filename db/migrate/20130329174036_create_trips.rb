class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.string :name
      t.string :code
      t.decimal :price, :precision => 8, :scale => 2
      t.date :start_date
      t.date :end_date
      t.date :expiration_date
      t.string :city
      t.string :state

      t.integer :goal

      t.timestamps
    end
    add_index :trips, :code,                :unique => true
  end
end
